import { LIST_POST_PUBLIC } from '../types';

function getPostPublic(data) {
  return (dispatch, getState) => {
    dispatch( { type: LIST_POST_PUBLIC, payload: data.data, status: data.http_code } )
  }
}

export {
  getPostPublic,
}
