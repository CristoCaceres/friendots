import { combineReducers } from 'redux';
import posts from './reducers/post';

const rootReducer = combineReducers({
  posts,
});

export default rootReducer;
