// services API
// utils
const HOST = 'https://testing.friendots.com';
const TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjU4ZmYzZjcwN2Q5MzQwNjY1YTQ3MDRiOCIsImlhdCI6MTUwMjgyMDU4MSwiZXhwIjoxNTA0MTE2NTgxfQ.oIUF1hHODESljx82fXvy3ZisLuJxwm8hqC8tlDeIbUw';
const USER_ID = '58f3b4e4e30edb4534f43f44k';

// services
const API_SUPPORT = '/support';
const API_POST_PUBLIC = '/post/getPostsPublic';

export {
  HOST,
  TOKEN,
  USER_ID,
  API_SUPPORT,
  API_POST_PUBLIC,
}
