import { LIST_POST_PUBLIC } from '../types';

const initialState = {
  list: [],
}

export default (state = initialState, action) => {
  switch (action.type) {
    case LIST_POST_PUBLIC:
      return {...state, list: action.payload, statusCode: action.status };
    default:
      return state;
  }
}
