import React, { Component } from 'react';
import { HOST, API_POST_PUBLIC, TOKEN, USER_ID } from '../api/services';
import CircularProgress from 'material-ui/CircularProgress';
import axios from 'axios';
import { connect } from 'react-redux';
import { getPostPublic } from '../api/actions/post';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import ReactDOM from 'react-dom';
var $ = require("jquery");
var AWS = require('aws-sdk');
//AWS.config.loadFromPath(path.join('.', 'config.json'))
AWS.config.update({
 accessKeyId: '#',
 secretAccessKey: '#',
 region: 'us-east-1',
});
var s3 = new AWS.S3({apiVersion: '2006-03-01'});

class PressPosts extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoad: true,
      isOpen: false,
      isDownload: false,
      selectPost: {},
      imageAWS: [],
    };
    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.getAWSImages = this.getAWSImages.bind(this);
    this.renderImage = this.renderImage.bind(this);
  }

  handleOpen(post) {
    this.setState({
      selectPost: post,
      isOpen: true,
    });
  };

  handleClose() {
    this.setState({isOpen: false});
  };

  componentDidUpdate() {
    if (this.state.isLoad === true) {
      if (this.state.isDownload === false) {
        if (this.props.posts.length > 0) {
          this.setState({
            isDownload: true,
          });
          this.getAWSImages(this.props.posts);
        }
      } else if (this.state.imageAWS.length === this.props.posts.length) {
        this.setState({
          isLoad: false,
        });
      }
    }
  }

  componentWillMount() {
    const config = {
      headers: {
          'x-access-token': TOKEN,
      }
    };
    axios.post( HOST+API_POST_PUBLIC , {
      user_id: USER_ID,
    }, config)
    .then((response) => {
      this.props.getPostPublic(response.data);
    })
    .catch((error) => {
      console.log(error);
    });
  }

  getAWSImages(posts) {
    posts.map((post, index) => {
      if (post.type === 1 ) {
        const urlParams = {Bucket: 'frieends', Key: post.url_large};
        s3.getSignedUrl('getObject', urlParams, (err, url) => {
          if (err) {
            console.log(err);
          }
          let images = this.state.imageAWS;
          const element = {
            id: post._id,
            url: url,
          }
          images.push(element);
          this.setState({
            imageAWS: images,
          });
        });
      } else if (post.type === 2) {
        const urlParams = {Bucket: 'frieends', Key: post.thumbnail};
        s3.getSignedUrl('getObject', urlParams, (err, url) => {
          if (err) {
            console.log(err);
          }
          let images = this.state.imageAWS;
          const element = {
            id: post._id,
            url: url,
          }
          images.push(element);
          this.setState({
            imageAWS: images,
          });
        });
      } else if (post.type === 3) {
        const urlParams = {Bucket: 'frieends', Key: post.url_large+".jpg"};
        s3.getSignedUrl('getObject', urlParams, (err, url) => {
          if (err) {
            console.log(err);
          }
          let images = this.state.imageAWS;
          const element = {
            id: post._id,
            url: url,
          }
          images.push(element);
          this.setState({
            imageAWS: images,
          });
        });
      } else if (post.type === 4) {
        let images = this.state.imageAWS;
        const element = {
          id: post._id,
          url: post.thumbnail,
        }
        images.push(element);
        this.setState({
          imageAWS: images,
        });
      } else if (post.type === 5) {
        const urlParams = {Bucket: 'frieends', Key: post.thumbnail};
        s3.getSignedUrl('getObject', urlParams, (err, url) => {
          if (err) {
            console.log(err);
          }
          let images = this.state.imageAWS;
          const element = {
            id: post._id,
            url: url,
          }
          images.push(element);
          this.setState({
            imageAWS: images,
          });
        });
      }
    });
  }

  renderImage(id, index) {
    let url;
    for (var i = 0; i < this.state.imageAWS.length; i++) {
      if (this.state.imageAWS[i].id === id) {
        url = this.state.imageAWS[i].url;
        break;
      }
    }
    return (
      <div className="box-image">
        <img src={url}/>
      </div>
    );
  }

  render() {
    const style = {
      circularBar: {
        position: 'absolute',
        marginTop: '-40px',
        marginLeft: '-40px',
        top: '50%',
        left: '50%',
      }
    };
    if (this.state.isLoad) {
      return (
        <CircularProgress size={80} thickness={5} style={style.circularBar}/>
      );
    } else {
      const actions = [
        <FlatButton
          label="BACK"
          primary={true}
          onClick={this.handleClose}
        />,
      ];
      return (
        <div className="press-center-boxes">
          {this.props.posts.map((row, index) =>
            <div className="press-boxes" onClick={() => this.handleOpen(row, index)}>
              {this.renderImage(row._id)}
              <div className="box-description">
                {row.title}
              </div>
            </div>
          )}
          <Dialog
            title={this.state.selectPost.title}
            actions={actions}
            modal={true}
            open={this.state.isOpen}
          >

          </Dialog>
        </div>
      );
    }
  }
}

function mapStateToProps(state) {
  return {
    posts: state.posts.list,
  };
}

export default connect (mapStateToProps, {getPostPublic})(PressPosts);
