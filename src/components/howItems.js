import React, { Component } from 'react';

class HowItems extends Component {

  constructor(props) {
    super(props);
    this.state = {
      play1: false,
    };
    this.play1 = this.play1.bind(this);
    this.handlePlay1On = this.handlePlay1On.bind(this);
    this.handlePlay1Off = this.handlePlay1Off.bind(this);
  }

  handlePlay1On() {
    this.setState({
      play1: true,
    });
  }

  handlePlay1Off() {
    this.setState({
      play1: false,
    });
  }

  play1() {
    if (this.state.play1) {
      return (
        <div className="how-item" onMouseEnter={this.handlePlay1On} onMouseLeave={this.handlePlay1Off}>
          <video width="100%" height="100%" autoPlay controls>
            <source src="videos/example.mp4" type="video/mp4"/>
            Your browser does not support the video tag.
          </video>
        </div>
      );
    } else {
      const image = 'images/rectangle.png';
      const item = {
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center center',
        backgroundImage: 'url('+ image + ')',
        backgroundSize: '100% 100%',
      }
      return (
        <div className="how-item" style={item} onMouseEnter={this.handlePlay1On} onMouseLeave={this.handlePlay1Off}>
          <div className="how-description">
            HOW CONNECT YOUR FRIENDS
          </div>
        </div>
      );
    }
  }

  render() {
    return (
      <div className="how-box">
        {this.play1()}
        <div className="how-item">

        </div>
        <div className="how-item">

        </div>
        <div className="how-item">

        </div>
        <div className="how-item">

        </div>
      </div>
    );
  }
}

export default HowItems;
