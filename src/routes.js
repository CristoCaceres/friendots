import React from 'react';
import {
    Router,
    Route,
    browserHistory,
    IndexRoute
} from 'react-router';

import App from './App';
import Support from './pages/support';
import Privacy from './pages/privacy';
import Terms from './pages/terms';
import How from './pages/how';

const renderRoutes = () => (
  <Router history={browserHistory}>
      <Route component={App}>
        <Route path="/"/>
        <Route path="/press"/>
        <Route path="/download"/>
        <Route path="/how"/>
        <Route path="/support" component={Support}/>
        <Route path="/privacy" component={Privacy}/>
        <Route path="/terms" component={Terms}/>
      </Route>
  </Router>
);

export default renderRoutes;
