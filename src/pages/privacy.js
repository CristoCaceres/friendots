import React, { Component } from 'react';

class Privacy extends Component {
  constructor(props) {
    super(props);
  }
  render () {
    return (
      <div className="page privacy">
        <div className="dot-press"></div>
        <div className="friend-dot-head"></div>
        <div className="background-big-messages top-message">PRIVACY</div>
        <div className="background-big-messages bottom-message">PRIVACY</div>
        <div className="grid">
            <div className="col-xs-6 col-xs-offset-3 box-info">
              <div className="info">
                <h3>PRIVACY POLICY</h3>
                <p>Vivo RL, LLC (&ldquo;Vivo,&rdquo; &ldquo;we,&rdquo; or &ldquo;us&rdquo;) knows you care how information about you is collected, used, shared, and protected. This notice describes our current privacy policy (the &ldquo;Policy&rdquo;), which applies to everyone using the Friendots mobile application or visiting www.friendots.com (collectively the &ldquo;Service&rdquo;). If you do not accept the terms of this Policy and the referenced documents, do not continue using the Service. <br />
                <br />
                This Policy outlines:<br />
                1. What Information We Collect About You<br />
                2. How We Use and Share that Information<br />
                3. How We Protect that Information and How You Can Help<br />
                4. What Choices You Have<br />
                <br />
                <h5>What Personal Information Does Vivo Gather About You?</h5>
                Some types of information gathered include:<br />
                Information You Provide: We receive and store information you submit through use of our Service or give us in any way. Such information includes, but is not limited to, information you provide when registering, such as your name, address, phone number, email address, birthday, and user profile information (including login and password details, and any photos); and your business name and business address (the &ldquo;User Information&rdquo;). Other such information includes, but is not limited to, other content you upload, such as photos, comments, posts, and other material of any kind (&ldquo;User Generated Content&rdquo;)(collectively, &ldquo;User Content&rdquo;). This information is used to enhance and customize your experience with the Service, to respond to your requests, and to communicate with you. You can choose not to provide certain information, but then you might not be able to take full advantage of many of our features.<br />
                Automatic Information: We receive and store information automatically when you interact with us through use of &ldquo;cookies&rdquo; and other similar technology. You can disable cookies at any time. By doing so, the Service will not be able to recognize you as a registered user and will affect your experience. <br />
                E-mail Communications: If your computer supports such capabilities, we may receive confirmation when you open e-mails we send you. If you do not want to receive e-mail from us, please adjust your email preferences or unsubscribe by clicking the unsubscribe link at the bottom of any of our e-mails.<br />
                Information from Other Sources: We may also receive information about you from other sources, including third-party website analytic tools, which collect information about visitor traffic; log file information generated when you access or use the Service; device and location identifiers if you access the Service using a mobile device; and other metadata. We may also receive and store information from your contacts or other social network sites. <br />
                <br />
                <h5>How Does Vivo Use and Share This Information?</h5>
                We may use and share your information (including aggregate and anonymized information) as follows:<br />
                Improve User Experience: We may monitor and analyze activity, metrics, and trends, and use your information to develop, maintain, and improve your user experience; to streamline the Service by storing your access credentials and log file; and to make suggestions and provide a customized experience via research and development, marketing and advertising. We may also use information to fix technical errors, update the Service, and communicate with you.<br />
                Third-Party Service Providers: We may employ other companies (including affiliates within our network) to perform functions on our behalf. These service providers and affiliate companies have access to personal and business information needed to perform their functions, but are not authorized to use it for other purposes. We do not provide any of your non-public information (such as payment information, password details, or your IP address) to third parties without your consent, unless required by law.<br />
                Protection of Vivo and Others: We will use collected information to enhance the safety and security of the Service, related products, and our other users. In addition, we reserve the right to disclose information and User Content when we believe that doing so is reasonably necessary to comply with the law or law enforcement, to prevent fraud or abuse, or to protect Vivo&rsquo;s legal rights.<br />
                In a Merger or Sale: Your information may be shared in the event of a potential sale, merger, acquisition, or other change of ownership or control of Vivo, whether in part or whole, and your information may be included as a company asset. In such event, you will remain the owner of your User Content.<br />
                <br />
                <h5>How Does Vivo Protect Your Information and How Can You Help?</h5>
                Vivo works to protect your information during transmission by using Secure Sockets Layer (&ldquo;SSL&rdquo;) software, which encrypts information you input. SSL is used to prevent unauthorized access, maintain data accuracy, and to ensure the correct usage of information.<br />
                You agree that information you provide can be seen by others and used by us as described in this Privacy Policy and our Terms and Conditions (as they may change from time to time) which are hereby incorporated by reference in their entirety.<b> ANY INFORMATION AND CONTENT YOU POST THROUGH USE OF THE SERVICE WILL BE PUBLIC AND SEEN BY OTHERS</b>. Keep in mind that any User Content shared can be saved and re-shared by others. As with all digital platforms and the Internet at large, we cannot control and are not responsible for how others use your information and User Content. You can control accessibility of your User Content by adjusting your Privacy Settings. We will never ask for your username and password, and you should not share such information with others. Remember to sign off after using any shared computer.<br />
                <br />
                <h5>What Choices Do You Have?</h5>
                You may access a broad range of information about your account through the Service. You may update your information or deactivate and delete your account at anytime, including your user profile, privacy and account settings, and, in most cases, your User Content. <br />
                <br />
                <h5>Are Children Allowed to Use the Website?</h5><br />
                The Service is intended for people over 13 years of age. Users under the age of 13 may only use the Service under parent or guardian supervision. We do not knowingly collect, share, or store any information from users under 13 years of age who are not supervised by a parent or guardian. <br />
                <br />
                <h5>Conditions of Use, Notices, and Modifications</h5><br />
                If you choose to use the Service, your visit, use, or any dispute over privacy is subject to this Privacy Policy and our Terms and Conditions, including limitations on damages, resolution of disputes, and application of the law of the State of Florida. If you have any concern about privacy relating to the Service, please email us at [email]. Vivo reserves the right to change the Service and the terms of this Policy at any time. If modifications are made, we will provide you notice of such modifications and when they go into effect. It is your responsibility to review these notices and any modifications to this Policy. If you use (or continue to use) the Service after a change, that means you accept the new Policy terms.</p>
              </div>
            </div>
        </div>
      </div>
    );
  }
}

export default Privacy;
