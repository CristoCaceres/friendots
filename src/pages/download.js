import React, { Component } from 'react';

class Download extends Component {
  constructor(props) {
      super(props)
  }

  render() {
    return (
      <div>
        <div className="background-big-messages top-message">DOWNLOAD</div>
        <div className="background-big-messages bottom-message">DOWNLOAD</div>
        <div className="dot-download"></div>
        <div className="friend-dot-download-logo"></div>
        <div className="apple-store"></div>
        <div className="text-footer-download">©2017 FRIENDOTS, INC</div>
        <div className="sticker-1"></div>
        <div className="sticker-2"></div>
        <div className="sticker-3"></div>
        <div className="sticker-4"></div>
        <div className="sticker-5"></div>
        <div className="sticker-6"></div>
        <div className="sticker-7"></div>
        <div className="sticker-8"></div>
        <div className="sticker-9"></div>
        <div className="sticker-10"></div>
        <div className="sticker-11"></div>
        <div className="sticker-12"></div>
        <div className="sticker-13"></div>
        <div className="sticker-14"></div>
        <div className="sticker-15"></div>
        <div className="sticker-16"></div>
        <div className="sticker-17"></div>
        <div className="sticker-18"></div>
        <div className="sticker-19"></div>
        <div className="sticker-20"></div>
        <div className="sticker-21"></div>
        <div className="sticker-22"></div>
        <div className="sticker-23"></div>
        <div className="sticker-24"></div>
        <div className="sticker-25"></div>
        <div className="sticker-26"></div>
        <div className="sticker-27"></div>
        <div className="sticker-29"></div>
        <div className="sticker-30"></div>
        <div className="sticker-31"></div>
      </div>
    );
  }
}

export default Download;
