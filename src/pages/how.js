import React, { Component } from 'react';
import HowItems from '../components/howItems';

class How extends Component {
  constructor(props) {
    super(props);
  }
  render () {
    return (
      <div>
        <div className="dot-press"></div>
        <div className="friend-dot-head"></div>
        <div className="background-big-messages top-message">HOW</div>
        <div className="background-big-messages bottom-message">HOW YOU CAN
          <br/>CHANGE THE WORLD</div>
        <HowItems />
      </div>
    );
  }
}

export default How;
