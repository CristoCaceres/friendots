import React, { Component } from 'react';

class Home extends Component{
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
        <div className="background-big-messages top-message">WHO ARE YOUR FRIENDS</div>
        <div className="background-big-messages bottom-message">IT'S TIME TO CONNECT THE DOTS</div>
        <div className="cell"></div>
        <div className="hand"></div>
        <div className="dot-hand"></div>
        <div className="friend-dot-home-logo"></div>
      </div>
    );
  }
}

export default Home;
