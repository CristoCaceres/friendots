import React, { Component } from 'react';
import PressPosts from '../components/pressPosts';

class Press extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <div className="dot-press"></div>
        <div className="friend-dot-head"></div>
        <div className="background-big-messages top-message">PRESS</div>
        <div className="background-big-messages bottom-message">PRESS</div>
        <PressPosts />
      </div>
    );
  }
}

export default Press;
