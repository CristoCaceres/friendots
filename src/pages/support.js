import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import validator from 'validator';
import { HOST, API_SUPPORT } from '../api/services';
import axios from 'axios';

const textErrorNameIsEmpty = 'Name is required';
const textErrorEmailIsEmpty = 'Email is required';
const textErrorEmailNotEmail = 'Email not format';
const textErrorSubjectIsEmpty = 'Subject is required';
const textErrorMessageLarge = 'Massage max of 350 characteres';

class Support extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      subject: '',
      message: '',
      errorName: '',
      errorEmail: '',
      errorSubject: '',
      errorMessage: '',
      success: '',
    }
    this.handleEmail = this.handleEmail.bind(this);
    this.handleSend = this.handleSend.bind(this);
  }

  handleSend() {
    if (validator.isEmpty(this.state.email)) {
      this.setState({
        errorEmail: textErrorEmailIsEmpty,
      });
    } else if (!validator.isEmail(this.state.email)) {
      this.setState({
        errorEmail: textErrorEmailNotEmail,
      });
    } else {
      // all fine
      axios.post( HOST+API_SUPPORT , {
        email: this.state.email,
      })
      .then((response) => {
        console.log(response);
        this.setState((prevState, props) => ({
          success: response.data.data,
          email: '',
          errorEmail: '',
        }));
      })
      .catch((error) => {
        this.setState({
          success: error,
        })
      });
    }
  }

  handleEmail(event) {
    if (validator.isEmpty(event.target.value)) {
      this.setState({
        errorEmail: textErrorEmailIsEmpty,
      });
    } else if (!validator.isEmail(event.target.value)) {
        this.setState({
          errorEmail: textErrorEmailNotEmail,
        });
    } else {
      this.setState({
        errorEmail: '',
      });
    }
    this.setState({
      email: event.target.value,
    });
  }

  render() {
    const style = {
      textField: {
        height: '36px',
        backgroundColor: '#000',
        lineHeight: '10px',
        borderRadius: '0.8vh 0px 0px 0.8vh',
      },
      textInput: {
        color: '#f9f9f9',
        webkitTextFillColor: '#fff',
        paddingLeft: '10px',
      },
      hint: {
        color: '#f9f9f9',
        paddingLeft: '10px',
      },
      button: {
        borderRadius: '0px 0.8vh 0.8vh 0px',
        backgroundColor: '#000',
        color: '#f9f9f9'
      },
      col: {
        paddingLeft: '1vh',
        paddingRight: '0px',
      },
      error: {
        marginTop: '1vh',
        marginLeft: '7px',
      }
    }
    return (
      <div className="page support">
        <div className="dot-press"></div>
        <div className="friend-dot-head"></div>
        <div className="background-big-messages top-message">HELLO</div>
        <div className="background-big-messages bottom-message">HELLO</div>
        <div className="support-form">
          <div className="grid">
            <div className="col-xs-6 col-xs-offset-3 box-support">
              <div className="col-xs-8" style={style.col}>
                <TextField
                  hintText="Email or user"
                  fullWidth={true}
                  value={this.state.email}
                  onChange={this.handleEmail}
                  errorText={this.state.errorEmail}
                  errorStyle={style.error}
                  style={style.textField}
                  inputStyle={style.textInput}
                  underlineStyle={{
                    display: 'none',
                  }}
                  hintStyle={style.hint}
                />
              </div>
              <div className="col-xs-4" style={style.col}>
                <RaisedButton
                  label="Send"
                  primary={true}
                  onClick={this.handleSend}
                  buttonStyle={style.button}
                />
              </div>
              <div className="col-xs-8 info">
                <p>Every day we work to improve this community and that way we all grow together.
                We apologize in advance for any inconvenience you may have, we will try to solve it in the best way,
                Send us your email or user name so that one of our representatives can contact you</p>
                <br />
                Thank you
                <br />
                Team Friendots.
              </div>
              <div>
                {this.state.success}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Support;
