import React from 'react';
import ReactDOM from 'react-dom';
import RenderRoutes from './routes';
import './index.css';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import promise from 'redux-promise';
import reducers from './api/reducers';

const createStoreWithMiddleware = applyMiddleware(thunk, promise)(createStore);

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
    <RenderRoutes />
  </Provider>,
  document.getElementById('app')
);
