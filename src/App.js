import React, { Component } from 'react';
import { Link } from 'react-router';
import Home from './pages/home';
import Press from './pages/press';
import Download from './pages/download';
import How from './pages/how';
import SwipeableViews from 'react-swipeable-views';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

//const themeFriendots = getMuiTheme({});
//console.log(themeFriendots);

class App extends Component {
  constructor(props) {
    super(props);

    let menu = 0;
    // Validate routes
    if (props.location.pathname === "/") {
      menu = 0;
    } else if (props.location.pathname === "/press") {
      menu = 1;
    } else if (props.location.pathname === "/download") {
      menu = 2;
    } else if (props.location.pathname === "/how") {
      menu = 3;
    } else if (props.location.pathname === "/privacy") {
      menu = 4;
    } else if (props.location.pathname === "/terms") {
      menu = 5;
    } else if (props.location.pathname === "/support") {
      menu = 6;
    } else {
      menu = 7;
    }

    this.state = {
      menu: menu,
      menuClass: {
        home: 'selected',
        press: '',
        download: '',
        support: '',
        privacy: '',
        terms: '',
        how: '',
      },
    }
    this.handleMenu = this.handleMenu.bind(this);
  }

  componentDidMount() {
    switch (this.state.menu) {
      case 0:
        this.setState({
          menuClass: {
            home: 'selected',
            press: '',
            download: '',
            support: '',
            privacy: '',
            terms: '',
            how: '',
          }
        });
        break;
      case 1:
        this.setState({
          menuClass: {
            home: '',
            press: 'selected',
            download: '',
            support: '',
            privacy: '',
            terms: '',
            how: '',
          }
        });
        break;
      case 2:
        this.setState({
          menuClass: {
            home: '',
            press: '',
            download: 'selected',
            support: '',
            privacy: '',
            terms: '',
            how: '',
          }
        });
        break;
      case 3:
        this.setState({
          menuClass: {
            home: '',
            press: '',
            download: '',
            how: 'selected',
            privacy: '',
            terms: '',
            support: '',
          }
        });
        break;
      case 4:
        this.setState({
          menuClass: {
            home: '',
            press: '',
            download: '',
            how: '',
            privacy: 'selected',
            terms: '',
            support: '',
          }
        });
        break;
      case 5:
        this.setState({
          menuClass: {
            home: '',
            press: '',
            download: '',
            how: '',
            privacy: '',
            terms: 'selected',
            support: '',
          }
        });
        break;
      case 6:
        this.setState({
          menuClass: {
            home: '',
            press: '',
            download: '',
            how: '',
            privacy: '',
            terms: '',
            support: 'selected',
          }
        });
        break;
      default:
        break;
    }
  }

  handleMenu(value) {
    this.setState({
      menu: value,
    });
    switch (value) {
      case 0:
        this.setState({
          menuClass: {
            home: 'selected',
            press: '',
            download: '',
            support: '',
            privacy: '',
            terms: '',
            how: '',
          }
        });
        break;
      case 1:
        this.setState({
          menuClass: {
            home: '',
            press: 'selected',
            download: '',
            support: '',
            privacy: '',
            terms: '',
            how: '',
          }
        });
        break;
      case 2:
        this.setState({
          menuClass: {
            home: '',
            press: '',
            download: 'selected',
            support: '',
            privacy: '',
            terms: '',
            how: '',
          }
        });
        break;
      case 3:
        this.setState({
          menuClass: {
            home: '',
            press: '',
            download: '',
            how: 'selected',
            privacy: '',
            terms: '',
            support: '',
          }
        });
        break;
      case 4:
        this.setState({
          menuClass: {
            home: '',
            press: '',
            download: '',
            how: '',
            privacy: 'selected',
            terms: '',
            support: '',
          }
        });
        break;
      case 5:
        this.setState({
          menuClass: {
            home: '',
            press: '',
            download: '',
            how: '',
            privacy: '',
            terms: 'selected',
            support: '',
          }
        });
        break;
      case 6:
        this.setState({
          menuClass: {
            home: '',
            press: '',
            download: '',
            how: '',
            privacy: '',
            terms: '',
            support: 'selected',
          }
        });
        break;
      default:
        break;
    }
  }

  render() {
    return (
      <MuiThemeProvider>
        <div>
          <div className="menu">
            <ul>
              <Link className={this.state.menuClass.home} to="/" onClick={() => this.handleMenu(0)}>
                HOME
              </Link>
              <Link className={this.state.menuClass.press} to="/press" onClick={() => this.handleMenu(1)}>
                PRESS
              </Link>
              <Link className={this.state.menuClass.download} to="/download" onClick={() => this.handleMenu(2)}>
                DOWNLOAD
              </Link>
              <Link className={this.state.menuClass.how} to="/how" onClick={() => this.handleMenu(3)}>
                HOW
              </Link>
            </ul>
          </div>
          <div className="bottom-section">
            <div className="bottom-menu">
              <ul>
                <Link to="/privacy" className={this.state.menuClass.privacy} onClick={() => this.handleMenu(4)}>
                  PRIVACY
                </Link>
                <Link to="/terms" className={this.state.menuClass.terms} onClick={() => this.handleMenu(5)}>
                  TERMS
                </Link>
                <Link to="/support" className={this.state.menuClass.support} onClick={() => this.handleMenu(6)}>
                  SUPPORT
                </Link>
              </ul>
            </div>
            <div className="copyright">
              ©2017 Friendots. All Rights Reserved.
            </div>
          </div>
          <div>
            {this.state.menu < 4 &&
              <SwipeableViews
                index={this.state.menu}
                onChangeIndex={this.handleMenu}>
                <div className="home">
                  <Home />
                </div>
                <div className="press">
                  <Press />
                </div>
                <div className="download">
                  <Download />
                </div>
                <div className="how">
                  <How />
                </div>
              </SwipeableViews>
            }
            {this.props.children}
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default App;
